<h1>PANDORA</h1>

A telegram bot implemented in entirely nim in order for me to
familiarize myself with this awesome language.

> For now this bot doesn't do much, but its honest work

# Usage

Before we start, make sure you have [nim](https://nim-lang.org) and
nimble package manager installed.

In order to use the bot, you need to first compile it:

```sh
nimble build
```

This should've made an executalbe called `pandora`(or `pandora.exe`
on windows) in the `./bin` directory.

For it's work, bot requires a telegram bot token. You can aquire one
from [BotFather](https://t.me/BotFather). Once you have it, you'll
have to put it into `TELEGRAM_BOT_TOKEN` environment variable.
The easiest way to do this is create a file called `.env` in the
bot working directory with the content as follows:

```sh
TELEGRAM_BOT_TOKEN="0000000000:AAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
```

Where `0000000000:AAAAAAAAAAAAAAAAAAAAAAAAAAAAA` is the token
BotFather gave to you.

Once its all done, you can run the bot executalbe.