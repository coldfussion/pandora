# Package

version       = "0.1.0"
author        = "Crystal Melting Dot"
description   = "A Telegram bot written in nim"
license       = "AGPL-3.0-only"
srcDir        = "src"
bin           = @["pandora"]
binDir        = "bin"

# Dependencies

requires "nim >= 1.6.0"
requires "telebot >= 1.0.11"
requires "dotenv >= 1.1.0"