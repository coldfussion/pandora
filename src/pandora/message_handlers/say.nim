import std/[
    asyncdispatch,
    options,
    strutils
]

import telebot


proc handle_say*(bot: TeleBot, m: Message): Future[void] {.async.} =
    if not m.text.isSome:
        return

    var text = m.text.get
    for e in m.entities.get:
        if e.offset == 0 and e.kind == "bot_command":
            text = text[e.length..text.len-1]
            break
    
    if text.isEmptyOrWhitespace:
        return

    discard await bot.deleteMessage($m.chat.id, m.messageId)
    if m.replyToMessage.isSome:
        discard await bot.sendMessage(
            m.chat.id,
            text,
            replyToMessageId=m.replyToMessage.get.messageId
        )
    else:
        discard await bot.sendMessage(
            m.chat.id,
            text
        )