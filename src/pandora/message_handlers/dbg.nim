import std/[
    asyncdispatch,
    options,
    json
]

import telebot


proc handle_dbg*(bot: TeleBot, m: Message): Future[void] {.async.} =
    var target = m
    if m.replyToMessage.isSome:
        target = m.replyToMessage.get()[]

    if target.text.isSome:
        if target.text.get.len > 20:
            target.text = some(target.text.get[0..20] & "...")

    let text = (%target).pretty(3)
    discard await bot.sendMessage(
        m.chat.id,
        text="```\n" & text & "\n```",
        parseMode="markdown"
    )
