import std/[
    asyncdispatch,
    options
]

import telebot


proc handle_start*(bot: TeleBot, m: Message): Future[void] {.async.} =
    discard await bot.sendMessage(
        m.chat.id,
        "Hello, I'm <b>Pandora</b>!\n\n" &
        "A mutifunctional telegram bot implemented in nim." &
        "You can see my source code <a href=\"https://codeberg.org/coldfussion/pandora\">here</a>",
        parseMode="html",
        replyToMessageId = m.messageId
    )
