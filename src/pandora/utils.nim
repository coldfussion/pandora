import std/[options, strutils, asyncdispatch]

import telebot


proc build_user_string_repr*(user: User): string =
    ## Builds a string representation of given User
    ## in form of `firstName lastName (username | id)`.
    ## Primarily used for logging purposes
    var user_str = user.firstName
    
    if user.lastName.isSome:
        user_str &= " " & user.lastName.get

    user_str &= " ("
    if user.username.isSome:
        user_str &= user.username.get & " | " & user.id.intToStr
    else:
        user_str &= user.id.intToStr

    user_str &= ")"

    return user_str


proc register_my_commands*(bot: TeleBot): Future[void] {.async.} =
    discard await bot.setMyCommands(
        @[
            BotCommand(command: "/start", description: "general info")
        ]
    )
