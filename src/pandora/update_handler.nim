import std/[
    asyncdispatch,
    options,
    logging,
    strutils
]

import telebot


import ./utils
import ./message_handlers/[
    start, dbg, say
]


proc updateHandler*(b: Telebot, u: Update): Future[bool] {.async.} =
    if not u.message.isSome:
        # return true will make bot stop process other callbacks
        return true
    
    var response = u.message.get

    if response.fromUser.isSome:
        info("Got message from $1 : \"$2\"" % [$build_user_string_repr(response.fromUser.get), response.text.get])

    if response.entities.isSome:
        let entities = response.entities.get
        for e in entities:
            if e.kind == "bot_command" and e.offset == 0:
                var command = response.text.get()[0 .. e.length - 1].toLower

                if command.contains("@"):
                    let command_bot_name = command.split("@", maxsplit=1)
                    let me = await b.getMe
                    if command_bot_name[1] != me.username.get:
                        return true
                    command = command_bot_name[0]

                case command
                of "/start":
                    await handle_start(b, response)
                    return true
                of "/dbg":
                    await handle_dbg(b, response)
                    return true
                of "/say":
                    await handle_say(b,response)
                    return true

    return true
    
