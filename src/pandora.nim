import std/[os, logging]

import telebot, dotenv

import ./pandora/[
    update_handler,
    exceptions,
]

# Setup logging handlers
const log_format = "[$datetime] - $levelname - "
var FL = newRollingFileLogger("pandora.log", levelThreshold=lvlInfo, fmtStr=log_format)
var L = newConsoleLogger(lvlInfo, fmtStr=log_format)

addHandler(L)
addHandler(FL)


proc main() =
    # Load .env file and read token
    let env = initDotEnv()
    env.overload()

    let token_env = "TELEGRAM_BOT_TOKEN"
    if not os.existsEnv(token_env):
        raise ConfigurationDefect.newException("No token found in env")

    info("Starting bot...")
    
    let bot = newTeleBot(os.getEnv(token_env))
    bot.onUpdate(update_handler.updateHandler)
    bot.poll(timeout=300)


when isMainModule:
    main()
